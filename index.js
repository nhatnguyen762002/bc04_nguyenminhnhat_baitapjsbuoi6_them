function kiemTraSNT(x) {
  for (var j = 2; j <= Math.sqrt(x); j++) {
    if (x % j == 0) {
      return 0;
    }
  }

  return 1;
}

function xuatKetQua() {
  var soValue = document.getElementById("txt-so").value * 1;
  var ketQuaEl = document.getElementById("txt-ket-qua");

  ketQuaEl.innerHTML = "";
  ketQuaEl.classList.add("alert", "alert-dark");

  for (var i = 2; i <= soValue; i++) {
    var ktSNT = kiemTraSNT(i);

    if (ktSNT == 1) {
      ketQuaEl.innerHTML += `${i}, `;
    }
  }
}
